// A grid of tiles
function Grid(options) {
    this.width = options.width;
    this.height = options.height;
    this.tile = options.tile;

    this.clear();
}

Grid.prototype.at_edge = function(x, y) {
    return (x == 0 || x == this.width - 1 ||
            y == 0 || y == this.height - 1);
}

Grid.prototype.random = function() {
    return {'x': this.random_x(), 'y': this.random_y()};
}

Grid.prototype.random_x = function() {
    return Math.floor(Math.random() * (this.width - 1) + 1);
    return Math.floor(Math.random() * this.width);
}

Grid.prototype.random_y = function() {
    return Math.floor(Math.random() * (this.height - 1) + 1);
    return Math.floor(Math.random() * this.height);
}

Grid.prototype.get_width = function() {
    return this.width * this.title.width;
}

Grid.prototype.get_height = function() {
    return this.height * this.title.height;
}

// Creates an empty grid array
Grid.prototype.clear = function() {
    this.locations = new Array(this.width)
    for (var x = 0; x < this.width; x++) {
        this.locations[x] = new Array(this.height);
        // for (var y = 0; y < this.height; y++) {
        //    this.locations[x][y] = false;
        // }
    }
}

// Is the grid occupied at this location?
Grid.prototype.is_empty = function(x, y) {
    return !this.locations[x][y];
}

Grid.prototype.load_level = function(level) {
    this.clear();

    // Load level metadata
    if (level.meta.player === undefined) {
        var x = Math.floor(this.width / 2);
        var y = Math.floor(this.height / 2);
    } else {
        var x = level.meta.player.x;
        var y = level.meta.player.y;
    }

    // Place the player
    this.place_entity_at('Player', x, y);

    this.place_walls();
    
    // Load the level entities
    for (name in level.entities) {
        this.place_entities_at(name, level.entities[name].positions);
        this.place_multiple_entities(name, level.entities[name].amount);
        this.place_entities_randomly(name, level.entities[name].chance)
    }
}

// Create an instance of the entity at the given location
Grid.prototype.place_entity_at = function(name, x, y, log) {
    if (!log === false) {
        console.log("Placing entity '"+name+"' at", x, ",", y);
    }
    this.locations[x][y] = Crafty.e(name).at(x, y);
}

// Place an instance of the entity in the listed positions
Grid.prototype.place_entities_at = function(name, locations) {
    locations = locations || new Array();
    locations.forEach(function(position){
        this.place_actor(entity_name, position.x, position.y);
    })
}

// Place a set number of the entity at random locations
Grid.prototype.place_multiple_entities = function(name, amount) {
    amount = amount || 0;
    
    while(Crafty(name).length < amount) {
        var x = this.random_x();
        var y = this.random_y();
        if (this.is_empty(x, y)) {
            this.place_entity_at(name, x, y);
        }
    }
}

// Place entities randomly given a chance to appear at each location
Grid.prototype.place_entities_randomly = function(name, chance) {
    if (chance === undefined) { return; }
    for (var x = 0; x < this.width; x++) {
        for (var y = 0; y < this.height; y++) {
            if (this.is_empty(x, y) && Math.random() < chance) {
                this.place_entity_at(name, x, y);
            }
        }
    }
}

Grid.prototype.place_walls = function() {
    for (var x = 0; x < this.width; x++) {
        for (var y = 0; y < this.height; y++) {
            if (this.at_edge(x, y)) {
                this.place_entity_at('Wall', x, y, false);
            }
        }
    }
}

Game = {
    name: "This is a game",

    grid: new Grid({
        width: 49,
        height: 25,
        tile: {
            width:  14,
            height: 14
        },
    }),

    width: function() {
        return this.grid.width * this.grid.tile.width + this.grid.tile.width * 2;
    },

    height: function() {
        return this.grid.height * this.grid.tile.height + this.grid.tile.height * 2;
    },
    
    /*
     * Levels
     */

    get_level: function() {
        return Levels[this.get_level_number() - 1];
    },

    get_level_number: function () {
        return parseInt(localStorage.getItem('level'));
    },

    set_level_number: function (level) {
        return localStorage.setItem('level', level);
    },

    // Loads the current level
    load_level: function() {
        console.log("Loading level", this.get_level_number());
        this.grid.clear();
        this.grid.load_level(this.get_level());
    },

    start_level: function(level) {
        this.set_level_number(level);
        if (level > Levels.length) {
            Crafty.scene('Victory');
        } else {
            Crafty.scene('Intro');
        }
    },

    start_next_level: function() {
        this.start_level(this.get_level_number() + 1);
    },

    restart_level: function() {
        this.start_level(this.get_level_number());
    },

    // Initialise and start our game
    start: function() {
        if (localStorage.getItem('level') === null) {
            localStorage.setItem('level', 1);
        }

        Crafty.init(Game.width(), Game.height());
        Crafty.background('white');

        Crafty.bind('SceneChange', function(data){
            console.log("Scene:", data.newScene);
        })
        
        Crafty.scene('Loading');
    },

    /*
     * Achievements
     */

    achievement: function(name, description, counter) {
        var text = name + ": " + description;
        var achievements = this._get_localstorage('achievements');
        
        if (achievements[name] === undefined) {
            console.log("Awarding achievement: '"+text+"'");
            Crafty.audio.play('achievement');
            if ('show_achievement_text' in this) {
                this.show_achievement_text(text);
            }

            achievements[name] = description;
            this._set_localstorage('achievements', achievements);
        }
    },

    counted_achievement: function(name, description, n) {
        var counters = this._get_localstorage('achievement_counters');

        if (counters[name] === undefined) {
            counters[name] = 0;
        }

        counters[name]++;

        if (counters[name] >= n) {
            this.achievement(name, description);
        }

        this._set_localstorage('achievement_counters', counters);
    },

    _get_localstorage: function(name) {
        var data = localStorage.getItem(name);
        return data && JSON.parse(data) || {};
    },

    _set_localstorage: function(name, value) {
        return localStorage.setItem(name, JSON.stringify(value));
    }
}
