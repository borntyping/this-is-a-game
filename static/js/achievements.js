function show_achievements() {
    console.log("Reading achievements from localStorage");
    
    var element = document.getElementById('achievements');
    
    var achievements = localStorage.getItem('achievements');
    var achievements = JSON.parse(achievements) || {};
    
    for (name in achievements) {
        console.log("--> "+name+": '"+ achievements[name]+"'");
        var dt = document.createElement('dt');
        var dd = document.createElement('dd');
        dt.innerHTML = name;
        dd.innerHTML = achievements[name];
        element.appendChild(dt);
        element.appendChild(dd);
    }
}
