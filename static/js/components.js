/*
 * User interface components
 */

Crafty.c('Title', {
    init: function() {
        this.requires('2D, DOM, Text');

        this.attr({
            x: Game.grid.tile.width * 2,
            y: Game.grid.tile.height * 2,
            h: Game.height() - Game.grid.tile.height * 4,
            w: Game.width() - Game.grid.tile.width * 4,
        });
    },
});

Crafty.c('Notification', {
    init: function() {
        this.requires('2D, DOM, Text');
        
        this.attr({
            x: 0,
            y: Game.height() - Game.grid.tile.height,
            w: Game.width()
        });

        this.css({
            'line-height': Game.grid.tile.height + 'px',
            'font-size': (Game.grid.tile.height - 4) + 'px',
        });
    },
});

Crafty.c('Pause', {
    init: function() {
        this.requires('Title');

        this.attr({
            x: Game.grid.tile.width * 2,
            y: Game.grid.tile.height * 2,
            h: Game.height() - Game.grid.tile.height * 4,
            w: Game.width() - Game.grid.tile.width * 4,
        });

        this.text("<div><strong>Arrow keys</strong> to move<br/><strong>ESC</strong> to restart the current level<br/><strong>P</strong> to pause and show controls</div>");
    },
});

Crafty.c('Border', {
    init: function(){
        this.requires('2D, DOM, Text');

        this.attr({
            x: Game.grid.tile.width,
            y: Game.grid.tile.height,
            w: Game.grid.width * Game.grid.tile.width,
            h: Game.grid.height * Game.grid.tile.height,
        });
        
        this.css({
            'border-width': Game.grid.tile.height + 'px',
            'background': 'red',
        });
    },
})

/*
 * Gameplay components
 */

// The Grid component allows an element to be located on the grid
Crafty.c('Grid', {
    init: function() {
        this.attr({
            w: Game.grid.tile.width,
            h: Game.grid.tile.height
        })
    },

    at: function(x, y) {
        // Set the entity's position
        this.attr({
            x: x * Game.grid.tile.width + Game.grid.tile.width,
            y: y * Game.grid.tile.height + Game.grid.tile.height,
        });
        return this;
    }
});

// An "Actor" is an entity that is drawn onto the grid
Crafty.c('Actor', {
    init: function() {
        this.requires('2D, Canvas, Grid');

        // Rotate around the center
        this.origin("center")

        if (Game.debug === true) {
            this.requires('Color');
            this.color('rgba(0, 255, 0, 0.5)');
        }
    },
});

// Walls stop the player leaving the area
Crafty.c('Wall', {
    init: function() {
        this.requires('Actor, Color, Solid');
        this.color('black');
    },
});

// A "Block" is a solid element that stops the player
Crafty.c('Block', {
    init: function() {
        this.requires('Actor, sprite_block, Solid');
    },
});

// An "Enemy" kills the player on touch
Crafty.c('Enemy', {
    init: function() {
        this.requires('Actor, sprite_enemy');
    },
});

// A Pickup is a tile on the grid that the PC must visit in order to win the game
Crafty.c('Pickup', {
    init: function() {
        this.requires('Actor, sprite_pickup');
        
        this.bind("EnterFrame", function(data){
            this.rotation = this.rotation + 2 % 360;
        })
    },

    collect: function() {
        this.destroy();
        Crafty.audio.play('pickup');
        Game.achievement("Collect", "Collect your first pickup");
        Game.counted_achievement("Collect lots!", "Collect 10 pickups", 10);
        Crafty.trigger('PickupVisited', this);
    }
});

// This is the player-controlled character
Crafty.c('Player', {
    init: function() {
        this.requires('Actor, Fourway, sprite_player')
            .fourway(3);

        this.requires('Collision')
            .onHit('Pickup', this.visitPickup)
            .onHit('Solid', this.stopMovement)
            .onHit('Enemy', this.touchEnemy);

        this.bind('NewDirection', this.changeDirection);

        // Set the position correctly
        this.rotation = 270;
        this.crop(3, 2, 9, 11);
        this.origin("middle middle");
    },

    // Stop moving when a solid object is hit
    stopMovement: function() {
        Game.achievement("Bump", "Crash into a solid object");
        Game.counted_achievement("Bad driver", "Crash into a solid object 20 times", 20);
        
        this._speed = 0;
        if (this._movement) {
            this.x -= this._movement.x;
            this.y -= this._movement.y;
        }
    },

    // Called when the direction changes (!)
    changeDirection: function (e) {
        // http://stackoverflow.com/questions/6039522/given-the-x-and-y-velocity-of-an-object-how-can-the-angle-be-computed
        if (!(e.x == 0 && e.y == 0)) {
            this.rotation = Math.atan2(e.y, e.x)/(Math.PI/180);
        }
    },

    // Call .collect on the Pickup that was hit
    visitPickup: function(data) {
        data[0].obj.collect();
    },

    // Die!
    touchEnemy: function(data) {
        Crafty.audio.play('explosion');
        Crafty.scene('Death');
    },
});
