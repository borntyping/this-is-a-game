// Loading scene
// -------------
// Handles the loading of binary assets such as images and audio files
Crafty.scene('Loading', function(){
    Crafty.e('Title').text('Loading...')

    Crafty.load([
        'static/img/glyphicons-halflings.png',
        'static/sound/foom.mp3',
        'static/sound/foom.ogg',
        'static/sound/accept.mp3',
        'static/sound/accept.ogg',
        'static/sound/explosion.mp3',
        'static/sound/explosion.ogg',
        'static/sound/complete.mp3',
        'static/sound/complete.ogg',
        'static/sound/achievement.mp3',
        'static/sound/achievement.ogg',
    ]);

    Crafty.sprite(14, 'static/img/glyphicons-halflings.png', {
        sprite_player: [19, 3],
        sprite_pickup: [6, 0],
        sprite_enemy: [0, 5],
        sprite_block: [10, 0],
    }, 10, 10);

    Crafty.audio.add({
        intro: [
            'static/sound/foom.mp3',
            'static/sound/foom.ogg',
        ],
        
        pickup: [
            'static/sound/accept.mp3',
            'static/sound/accept.ogg',
        ],

        explosion: [
            'static/sound/explosion.mp3',
            'static/sound/explosion.ogg',
        ],

        complete: [
            'static/sound/complete.mp3',
            'static/sound/complete.ogg',
        ],

        achievement: [
            'static/sound/achievement.mp3',
            'static/sound/achievement.ogg',
        ],
    });

    Crafty.audio.play('intro');
    Crafty.scene("Title");
});

// Title page
// -------------
// Shows the name of the game
Crafty.scene('Title', function() {
    Crafty.e('Title').text(Game.name);

    setTimeout(function(){
        // Starts the current level >.<
        Game.restart_level();
    }, 1500);
});

// Level intro
// -------------
// Shows the level name before it starts
Crafty.scene('Intro', function() {
    Crafty.e('Title').text('This is level '+Game.get_level_number());

    setTimeout(function(){ Crafty.scene('Game') }, 1000);
});

// Game scene
// -------------
// Runs the core gameplay loop
Crafty.scene('Game', function() {
    Game.load_level();

    // Go to the next level if all of the pickups are collected
    this.next_level_binding = Crafty.bind('PickupVisited', function(e){
        if (Crafty('Pickup').length <= 0) {
            Game.start_next_level();
        }
    })

    /*
     * Display
     */

    // Shows the number and name of the current level
    var level_notification = Crafty.e('Notification').attr({y: 0});
    level_notification.text('Level '+Game.get_level_number()+': '+Game.get_level().meta.name);

    // Shows the last achievement
    var achievement_notification = Crafty.e('Notification')
        .attr({y: Game.height() - Game.grid.tile.height * 2})
        .css({"color": "white"});

    Game.show_achievement_text = function(text) {
        console.log("Showing achievement: '"+text+"'");
        achievement_notification.text(text);
        setTimeout(function() {
            if (achievement_notification.text() == text) {
                achievement_notification.text("");
            }
        }, 5000);
    }

    Game.achievement("Hello World", "Started playing");

    // Shows the progress through the level
    var progress_notification = Crafty.e('Notification');
    function set_progress_notification (){
        var num = Crafty('Pickup').length;
        var text = num + (num == 1 ? " pickup" : " pickups") + " left!";
        progress_notification.text(text);
    }
    set_progress_notification();
    this.pickup_binding = Crafty.bind(
        'PickupVisited', set_progress_notification)

    /*
     * Pause screen and keybindings
     */

    // Pause 
    this.pause_screen = Crafty.e('Pause').css({'display': 'none'});
    
    this.keybindings = this.bind('KeyDown', function(event) {
        if (event.key == Crafty.keys['P']) {
            Crafty.pause();
            if (Crafty.isPaused()) {
                this.pause_screen.css({'display': 'block'});
            } else {
                this.pause_screen.css({'display': 'none'});
            }
        } else if (event.key == Crafty.keys['ESC']) {
            // Restart the level when ESC is pressed
            return Game.restart_level();
        }
    })
}, function() {
    this.unbind('PickupVisited', this.pickup_binding);
    this.unbind('PickupVisited', this.next_level_binding);
    this.unbind('KeyDown', this.keybindings);

    delete Game.show_achievement_text;
});

// Death screen
// -------------
// Shown after the player dies
Crafty.scene('Death', function() {
    Crafty.e('Title').text("You died");
    Game.achievement("You died", "Die for the first time");
    Game.counted_achievement("You died a lot!", "Die 5 times", 5);
    Game.restart_level();
});


// Victory scene
// -------------
// Tells the player when they've won and lets them start a new game
Crafty.scene('Victory', function() {
    Crafty.audio.play('complete');
    Crafty.e('Title').text("You win!");
    Game.achievement("You win!", "Win the game");
    Game.counted_achievement("You win lots!", "Win the game 5 times", 5);
        
    // After a short delay, watch for the player to press a key,
    // then restart the game when a key is pressed
    var delay = true;
    setTimeout(function() { delay = false; }, 1000);
    this.restart_game = Crafty.bind('KeyDown', function() {
        if (!delay) {
            this.unbind('KeyDown', this.restart_game);
            Game.set_level_number(1);
            Crafty.scene('Title');
        }
    });
});
