Levels = [{
    meta: {
        name: "Introduction",
    },
    entities: {
        Pickup: {amount: 1},
    },
},{
    meta: {
        name: "Blocks",
    },
    entities: {
        Pickup: {amount: 2},
        Block: {amount: 10},
    },
},{
    meta: {
        name: "Enemies",
    },
    entities: {
        Pickup: {amount: 4},
        Enemy: {amount: 10},
    },
},{
    meta: {
        name: "More Blocks",
    },
    entities: {
        Pickup: {amount: 6},
        Block: {amount: 20, chance: 0.025},
    },
},{
    meta: {
        name: "More Enemies",
    },
    entities: {
        Pickup: {amount: 8},
        Enemy: {amount: 20, chance: 0.025},
    },
}]
